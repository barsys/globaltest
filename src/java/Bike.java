
import java.time.LocalTime;


public class Bike {

    private BikeType type;
    private boolean available;
    private double pricePerHour;
    private String location;
    private BikeColor mainColor;
    private BikeColor secondaryColor;
    private Person owner;
    private LocalTime rentDate;
    private LocalTime returnTime;

    void reserve(Person client) {
        if (available) {
            available = false;
            owner = client;
            rentDate = LocalTime.now();
            client.rezerwacje.add(this);
            System.out.println(" Wypożyczono " + rentDate);
        }
        else {
            System.out.println("Rower jest zajęty!");
        }
    }

    void payAndReturn(Person client) {
        if (client == owner) {
            returnTime = LocalTime.now();
            System.out.println("Zwrocono rower " + returnTime);
            available = true;
            owner = null;
            int totalTime = Math.abs(rentDate.getSecond() - returnTime.getSecond());
            double totalprice = pricePerHour * totalTime;
            System.out.println("Czas pracu to " +totalTime + " do zapłaty "+ totalprice);
            client.rezerwacje.remove(this);
            client.pay(totalprice);

        }
        else {
            System.out.println("Nie jesteś właścicielem !!!");
        }

    }

    public Bike() {
        this.type = BikeType.GORSKI;
        this.available = true;
        this.pricePerHour = 1.5;
        this.location = "baza";
        this.mainColor = BikeColor.BLACK;
        this.secondaryColor = BikeColor.BLACK;
    }

    Bike(BikeType type, boolean available, double pricePerHour, String location, BikeColor mainColor, BikeColor secondaryColor) {
        this.type = type;
        this.available = available;
        this.pricePerHour = pricePerHour;
        this.location = location;
        this.mainColor = mainColor;
        this.secondaryColor = secondaryColor;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "type=" + type +
                ", available=" + available +
                ", pricePerHour=" + pricePerHour +
                ", location='" + location + '\'' +
                ", mainColor=" + mainColor +
                ", secondaryColor=" + secondaryColor +
                '}';
    }

    BikeType getType() {
        return type;
    }

    boolean isAvailable() {
        return available;
    }

    double getPricePerHour() {
        return pricePerHour;
    }

    public String getLocation() {
        return location;
    }

    BikeColor getMainColor() {
        return mainColor;
    }

    public BikeColor getSecondaryColor() {
        return secondaryColor;
    }

    public Person getOwner() {
        return owner;
    }

    public LocalTime getRentDate() {
        return rentDate;
    }

    public LocalTime getReturnTime() {
        return returnTime;
    }

}
