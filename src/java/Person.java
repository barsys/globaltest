import java.util.ArrayList;
import java.util.List;

public class Person {

    private double moneyAmount;
    public List<Bike> rezerwacje = new ArrayList<>();

    public double getMoneyAmount(){
        return moneyAmount;
    }
    public void pay(double amount) {
        this.moneyAmount -= amount;
        if (this.moneyAmount<0) {
            System.out.println("Debet : " + moneyAmount);
        } else {
            System.out.println("Na koncie pozostało : "+ moneyAmount);
        }
    }
    public void logout() {
        if (this.moneyAmount>=0) {
            if (rezerwacje.isEmpty()) {
                System.out.println("Do widzenia !");
                System.exit(0);
            }
            else {
                System.out.println("nie oddano wszystkich rowerów");
            }
        } else {
            System.out.println(" Najpierw należy uregulować płatności");
        }
    }
    public void insertcoin(double amount) {
        this.moneyAmount += amount;
    }

    public Person(double moneyAmount) {
        this.moneyAmount = moneyAmount;
    }
}
