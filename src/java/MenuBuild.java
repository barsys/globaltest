import java.util.Scanner;


public class MenuBuild {
    private Scanner inlet = new Scanner(System.in);
    private BikeDatabase bikeSheet = new BikeDatabase();
    private int option;
    private String location;
    private Person person1;
    public void login() {
        System.out.println("Dzień dobry. Wpisz kwotę pieniędzy :");
        person1  = new Person(inlet.nextDouble());
        bikeSheet.addBikes(10);
    }
    public void mainmenu() {
        System.out.println("Wybierz opcje :");
        System.out.println("1 - własne rowery");
        System.out.println("2 - rezerwuj nowy");
        System.out.println("3 - ustaw lokację");
        System.out.println("4 - moje konto");
        System.out.println("5 - wyloguj");
        option = inlet.nextInt();
        switch (option) {
            case 1:
                this.bikeManager();
                break;
            case 2:
                this.bikesort();
                break;
            case 3:
                location = inlet.next();

                break;
            case 4:
                this.accountMenager();
                break;
            case 5:
                person1.logout();
                break;
            default:
        }


    }
    private void accountMenager() {

        System.out.println(" Moje konto : stan konta :" + person1.getMoneyAmount());
        System.out.println("1 - Doładuj konto");
        System.out.println("2- powrót");
        option = inlet.nextInt();
        switch (option) {
            case 1:
                System.out.println("Wprowazdz kwotę :");
                person1.insertcoin(inlet.nextDouble());
                System.out.println("Doładowano");
                break;
            default:
                break;
        }
    }
    private void bikeManager() {
        int i = 0;
        for (Bike bike : person1.rezerwacje) {
            System.out.println(i + "   " + bike.toString());
            i++;
        }
        System.out.println(" oddaj rower nr:  (wprowdz -1 aby wyjśc)");
        option = inlet.nextInt();
        if (option >= 0 && option<=i-1) {
            person1.rezerwacje.get(option).payAndReturn(person1);
        }

    }
    private void bikeRent(int sortbycolumn) {
        int i = 0;
        for (Bike bike : bikeSheet.getSortedBikeListBy(sortbycolumn)) {
           System.out.println(i +"   " + bike.toString());
           i++;
        }
        System.out.println("Wybierz rower : (wprowazdz -1 aby wrócić");
        option = inlet.nextInt();
        if (option >= 0 && option<=i-1) {
            bikeSheet.getSortedBikeListBy(sortbycolumn).get(option).reserve(person1);
        }

    }
    private void bikesort() {
        System.out.println("Sortuj listę rowerów po : (wprowdz -1 aby wrócic");
        System.out.println("1 - cena za godzinę");
        System.out.println("2 - kolor");
        System.out.println("3 - typ roweru");
        System.out.println("4 - dostępnośc");
        option = inlet.nextInt();
        if (option > 0) {
            bikeRent(option);
        }
    }
}
