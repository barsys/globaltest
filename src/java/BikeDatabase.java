import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class BikeDatabase {
    private Random randomizer = new Random();
    private List<Bike> bikelist = new ArrayList<>();
    private List<Bike> sorted = new ArrayList<>();

    public List<Bike> getBikelist() {
        return bikelist;
    }
    List<Bike> getSortedBikeListBy(int column) {
        switch (column) {
            case 1:
                sorted = bikelist.stream().sorted(Comparator.comparing(bike -> bike.getPricePerHour())).collect(Collectors.toList());
                break;
            case 2:
                sorted = bikelist.stream().sorted(Comparator.comparing(bike -> bike.getMainColor())).collect(Collectors.toList());
                break;
            case 3:
                sorted = bikelist.stream().sorted(Comparator.comparing(bike -> bike.getType())).collect(Collectors.toList());
                break;
            case 4:
                sorted = bikelist.stream().sorted(Comparator.comparing(bike -> bike.isAvailable())).collect(Collectors.toList());
                break;
            default:
                sorted = bikelist;

        }
        return sorted;
    }
    void addBikes(int quantity) {
        for (int i = 1 ; i<=quantity ; i++) {
            bikelist.add(createRandomBike());
        }
    }

    private Bike createRandomBike() {
        return new Bike(BikeType.values()[randomizer.nextInt(BikeType.values().length)],true,(double) randomizer.nextInt(30)/10 +2,"baza", BikeColor.values()[randomizer.nextInt(BikeColor.values().length)], BikeColor.values()[randomizer.nextInt(BikeColor.values().length)]);
    }
}
